/************************************
图像处理系统,ReadBitMap
*************************************/
#include"bm.h"
#include<math.h>

void main()
{
	int be = 2;
	long poitn = 0;
	BITMAP_PZ bm = CreMap_pz("11.bmp");
	BITMAP_PZ bm1 = CreMap_pz("22.bmp");

	int m1 = bm1.BitInfoHead.biWidth / 2 + 15;
	int n1 = bm1.BitInfoHead.biHeight / 2 - 45;
	int m = bm.BitInfoHead.biWidth / 2;
	int n = bm.BitInfoHead.biHeight / 2 - 50;

	initgraph(bm.BitInfoHead.biWidth>bm1.BitInfoHead.biWidth ? bm.BitInfoHead.biWidth : bm1.BitInfoHead.biWidth, bm.BitInfoHead.biHeight>bm1.BitInfoHead.biHeight ? bm.BitInfoHead.biHeight : bm1.BitInfoHead.biHeight);

	for (int j = 0; j < bm1.BitInfoHead.biHeight; j += 1)
	{
		for (int i = 0; i < bm1.BitInfoHead.biWidth; i += 1)
		{
			RGBQUAd rgbb1 = GetPix(&bm1, i, j);
			{
				putpixel(i, j, RGB(rgbb1.rgbRed, rgbb1.rgbGreen, rgbb1.rgbBlue));
			}
		}
	}
	for (int j = 0; j < bm.BitInfoHead.biHeight; j += 1)
	{
		for (int i = 0; i < bm.BitInfoHead.biWidth; i += 1)
		{
			RGBQUAd rgbb = GetPix(&bm, i, j);
			{
					putpixel(i + m1 - m, j + n1 - n, RGB(rgbb.rgbRed, rgbb.rgbGreen, rgbb.rgbBlue));
			}
		}
	}
	while (1)
	{
		if (MouseHit())
		{
			static int mx, my,k=0;
			MOUSEMSG msg = GetMouseMsg();
			if (msg.x>m1 - m&&msg.x<m1 - m + bm.BitInfoHead.biWidth&&msg.y>n1 - n&&msg.y < n1 - n + bm.BitInfoHead.biHeight)
			{
				for (int j = msg.y - 30; j < msg.y + 30;j++)
					for (int i = msg.x - 30; i < msg.x + 30; i++)
					{
						if (sqrt(pow((double)(j - msg.y), 2) + pow((double)(i - msg.x), 2)) <30)
						{
							RGBQUAd rgbb2 = GetPix(&bm1, i, j);
							putpixel(i , j , RGB(rgbb2.rgbRed, rgbb2.rgbGreen, rgbb2.rgbBlue));
						}
					}
				//复原代码
				if (k == 0)
				{
					k = 1;
					mx = msg.x;
					my = msg.y;
				}
				else
				{
					for (int p = my - 33; p < my + 33; p++)
						for (int q = mx - 33; q < mx + 33; q++)
						{
							if ((q>m1 - m&&q<m1 - m + bm.BitInfoHead.biWidth&&p>n1 - n&&p < n1 - n + bm.BitInfoHead.biHeight)&&sqrt(pow((double)(p - my), 2) + pow((double)(q - mx), 2)) <33 && sqrt(pow((double)(p - msg.y), 2) + pow((double)(q - msg.x), 2)) >30)
							{
								RGBQUAd rgbb = GetPix(&bm, q - (m1 - m), p - (n1 - n));
								putpixel(q, p, RGB(rgbb.rgbRed, rgbb.rgbGreen, rgbb.rgbBlue));
							}
						}
				}
				mx = msg.x;
				my = msg.y;
				//circle(msg.x, msg.y, 30);
			}
		}
	}

	RGBQUAd rgbp;

	CloseBM_pz(bm);	//关闭位图
	getchar();
	getchar();
	closegraph();
	printf("\n");
}

