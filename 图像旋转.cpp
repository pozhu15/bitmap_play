/************************************
图像处理系统,ReadBitMap
*************************************/
#include"bm.h"
#include<math.h>

int getLineSize(int x1, int y1, int x2, int y2)	//得到两点的线段长度
{
	return sqrt(pow((double)(x1 - x2), 2) + pow((double)(y1 - y2), 2));
}

static float ex_toRad(float a)	//角度转换为弧度
{
	return (a*3.14159265f) / 180;
}
void toSpin(int px, int py, int rx, int ry, int r, int* x, int* y)//点（x，y）绕（px，py）旋转指定角度r，得到旋转后的点坐标
{
	*x = (int)(px + (rx)*cos(ex_toRad(r)));
	*y = (int)(py + (ry)*sin(ex_toRad(r)));
}

void ToSpin(int px, int py, int rx, int ry, int r, int *x, int *y)	//点（x，y）绕（px，py）旋转指定角度r，得到旋转后的点坐标
{
	int r1 = r;
	double j = (double)r1 * 3.1415926 / 180;	//实际弧度
	*x = (rx - px)*cos(j) - (ry - py)*sin(j)+ px;
	*y = (ry - py)*cos(j)+ (rx - px)*sin(j)+ py;
}
bool isfill(BITMAP_PZ *bmp, int *fi, int x, int y)	//判断周围点是否有两个被选
{
	int count = 0;
	int b[6][2] = { { 1, -1 }, { -1, 1 }, { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 } };
	for (int j = 0; j < 6; j++)
	{
		if (fi[x + b[j][0] + (y + b[j][1])*bmp->BitInfoHead.biWidth] == 1)
			count++;
		if (count > 3)
			return true;
	}
	return false;
}

bool shibie(BITMAP_PZ *bmp, int *fill, int x, int y)	//识别
{
	int num = 0;
	int x1, y1;
	RGBQUAd rgbb = GetPix(bmp, x, y);
	int yuv = rgbb.rgbRed*0.299 + rgbb.rgbGreen*0.578 + rgbb.rgbBlue*0.114;
	for (int i = 0; i < 360; i += 60)
	{
		toSpin(x, y, 3, 3, i, &x1, &y1);
		if (x1<0 || y1<0 || x1>bmp->BitInfoHead.biWidth || y1>bmp->BitInfoHead.biHeight)
			continue;
		RGBQUAd rgbb1 = GetPix(bmp, x1, y1);
		if (rgbb1.rgbRed*0.299 + rgbb1.rgbGreen*0.578 + rgbb1.rgbBlue*0.114 - yuv>10)
			num++;
	}
	if (num > 4)
	{
		if (!isfill(bmp, fill, x, y))
		{
			fill[x + (y)*bmp->BitInfoHead.biWidth] = 1;
		}
		return true;
	}
	else
	{
		return false;
	}
}
void main()
{
	int be = 2;
	long poitn = 0;
	BITMAP_PZ bm = CreMap_pz("11.bmp");
	BITMAP_PZ bm1 = CreMap_pz("22.bmp");
	int *fill = (int*)malloc(bm.BitInfoHead.biWidth*bm.BitInfoHead.biHeight*sizeof(int));
	memset(fill, 0, bm.BitInfoHead.biWidth*bm.BitInfoHead.biHeight*sizeof(int));
	FILE *poit, *rgb;
	poit = fopen("poit.h", "w");
	rgb = fopen("rgb.h", "w");
	int m = bm.BitInfoHead.biWidth / 2;
	int n = bm.BitInfoHead.biHeight / 2;
	int x, y, z;
	int kk = 0;
	initgraph(bm.BitInfoHead.biWidth, bm.BitInfoHead.biHeight);
	for (int j = 0; j < bm.BitInfoHead.biHeight; j += 1)
	{
		for (int i = 0; i < bm.BitInfoHead.biWidth; i += 1)
		{
			RGBQUAd rgbb = GetPix(&bm, i, j);
			{
				//if (rgbb.rgbRed*0.299 + rgbb.rgbGreen*0.578 + rgbb.rgbBlue*0.114 <= 192)
				//if (shibie(&bm,fill,i,j))
				if (sqrt(pow((double)(i - m), 2) + pow((double)(j - n), 2)) < 130)
				{
					int jk, kl;
					ToSpin(m, n, i, j, 180, &jk, &kl);
					putpixel(jk, kl, RGB(rgbb.rgbRed, rgbb.rgbGreen, rgbb.rgbBlue));
				}
				kk++;
			}
		}
	}
	for (int i = 0; i < 360; i += 60)
	{
		int jk, kl;
		toSpin(m, n, 3, 3, i, &jk, &kl);
		//	putpixel(jk, kl, RGB(255, 5, 0));
	}

	RGBQUAd rgbp;
	fprintf(rgb, " poit number  %d", poitn);
	fclose(poit);
	fclose(rgb);
	CloseBM_pz(bm);	//关闭位图
	getchar();
	getchar();
	closegraph();
	printf("\n");
}

