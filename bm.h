#ifndef _BM_H
#define _BM_H

#include <string.h>   
#include <math.h>     
#include <stdio.h>     
#include <stdlib.h>     
#include <malloc.h>  
#include<graphics.h>

#define   WIDTHBYTES(bits) (((bits)+31)/32*4)  //用来计算位图的实际宽度并确保它为32的倍数  

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef long LONG;

//位图文件头信息结构定义  
//其中不包含文件类型信息（由于结构体的内存结构决定，要是加了的话将不能正确读取文件信息）  
typedef struct tagBITMAPFILEHEADEr{
	//	WORD bfType;			//标志文件格式（值“BM”），此处在后面分开读取了
	DWORD bfSize;           //位图文件大小  
	WORD   bfReserved1;     //保留字，不考虑  
	WORD   bfReserved2;     //保留字，同上  
	DWORD bfOffBits;        //实际位图数据的偏移字节数，即前三个部分长度之和  
}BITMAPFILEHEADEr;

//信息头BITMAPINFOHEADER，也是一个结构，其定义如下：  
typedef struct tagBITMAPINFOHEADEr{
	//public:  
	DWORD   biSize;             //指定此结构体的长度，为40  
	LONG    biWidth;            //位图宽  
	LONG    biHeight;           //位图高  
	WORD    biPlanes;           //平面数，为1  
	WORD    biBitCount;         //采用颜色位数，可以是1，2，4，8，16，24，新的可以是32  
	DWORD   biCompression;      //压缩方式，可以是0，1，2，其中0表示不压缩  
	DWORD   biSizeImage;        //实际位图数据占用的字节数  
	LONG    biXPelsPerMeter;    //X方向分辨率  
	LONG    biYPelsPerMeter;    //Y方向分辨率  
	DWORD   biClrUsed;          //使用的颜色数，如果为0，则表示默认值(2^颜色位数)  
	DWORD   biClrImportant;     //重要颜色数，如果为0，则表示所有颜色都是重要的  
}BITMAPINFOHEADEr;

//调色板Palette，当然，这里是对那些需要调色板的位图文件而言的。24位和32位是不需要调色板的。  
//（似乎是调色板结构体个数等于使用的颜色数。）  
typedef struct tagRGBQUAd{
	//public:  
	BYTE     rgbBlue; //该颜色的蓝色分量  
	BYTE     rgbGreen; //该颜色的绿色分量  
	BYTE     rgbRed; //该颜色的红色分量  
	BYTE     rgbReserved; //保留值  
}RGBQUAd;

typedef struct tagBITMAP_PZ{
	WORD fileType;			//标志文件格式（值“BM”），此处在后面分开读取了
	BITMAPFILEHEADEr   BitHead;		//位图文件头信息结构定义 
	BITMAPINFOHEADEr BitInfoHead;		//信息头BITMAPINFOHEADER结构 
	RGBQUAd* DataOfBmp;		//将位图数据转化为RGB数据的地址 
}BITMAP_PZ;

BITMAP_PZ CreMap_pz(char* filenmae);	//创建位图,从文件读取位图
RGBQUAd GetPix(BITMAP_PZ* bmp, int x, int y);	//获得位图上一点的颜色
void CloseBM_pz(BITMAP_PZ bmp);	//关闭位图

//int getLineSize(int x1, int y1, int x2, int y2);	//得到两点的线段长度
static double ex_toRad(double a);	//角度转换为弧度
//点(x,y)旋转指定弧度r，得到旋转后的坐标
void toSpin(int px, int py, int rx, int ry, int r, int* x, int* y);
void ToSpin(int px, int py, int rx, int ry, int r, int *x, int *y);	//点（x，y）绕（px，py）旋转指定角度r，得到旋转后的点坐标
struct Ponit
{
	double x;
	double y;
	Ponit(double a = 0, double b = 0)
	{
		x = a;
		y = b;
	}		 // constructor 
};
// 返回点(rx,ry)以点(px,py)为圆心逆时针旋转alpha(单位：角度)后所在的位置 
Ponit Rotate(double px, double py, double alpha, double rx, double ry);
bool isfill(BITMAP_PZ *bmp, int *fi, int x, int y);	//判断周围点是否有两个被选
bool shibie(BITMAP_PZ *bmp, int *fill, int x, int y);	//识别

//void showBmpHead(BITMAPFILEHEADEr* pBmpHead);	//显示文件头
//void showBmpInforHead(BITMAPINFOHEADEr* pBmpInforHead);	//显示位图信息头
//void showRgbQuan(RGBQUAd* pRGB);	//显示调色版信息


#endif