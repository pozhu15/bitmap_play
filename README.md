# bitmap_play

#### 介绍
c语言玩转位图，位图轮廓识别提取，图片转换为字符，鼠标擦图，图片扭曲，截取圆形图片，像素图片等操作

#### 软件架构
c语言封装读取位图，处理位图像素


#### 安装教程

1.  平台vs2013
2.  需要安装exsy，使用其中的graphics.h图形库
3.  vs中新建项目，编译运行即可

#### 使用说明

1.  基础库为bm.cpp，bm.h，来读取位图像素
2.  其他cpp文件是实现不同的功能的代码


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
