/************************************
图像处理系统,ReadBitMap
*************************************/
#include"bm.h"
#include<math.h>

void main()
{
	int be = 2;
	long poitn = 0;
	BITMAP_PZ bm = CreMap_pz("11.bmp");
	BITMAP_PZ bm1 = CreMap_pz("22.bmp");

	int m1 = bm1.BitInfoHead.biWidth / 2 + 15;
	int n1 = bm1.BitInfoHead.biHeight / 2 - 45;
	int m = bm.BitInfoHead.biWidth / 2;
	int n = bm.BitInfoHead.biHeight / 2 - 50;

	initgraph(bm.BitInfoHead.biWidth>bm1.BitInfoHead.biWidth ? bm.BitInfoHead.biWidth : bm1.BitInfoHead.biWidth, bm.BitInfoHead.biHeight>bm1.BitInfoHead.biHeight ? bm.BitInfoHead.biHeight : bm1.BitInfoHead.biHeight);

	for (int j = 0; j < bm1.BitInfoHead.biHeight; j += 1)
	{
		for (int i = 0; i < bm1.BitInfoHead.biWidth; i += 1)
		{
			RGBQUAd rgbb1 = GetPix(&bm1, i, j);
			if (sqrt(pow((double)(m1 - i), 2) + pow((double)(n1 - j), 2)) >50)
			{
				putpixel(i, j, RGB(rgbb1.rgbRed, rgbb1.rgbGreen, rgbb1.rgbBlue));
			}
			else
			{
				//if (i%2==0)
				putpixel(i, j, RGB(rgbb1.rgbRed, rgbb1.rgbGreen, rgbb1.rgbBlue));
			}
		}
	}
	for (int j = 0; j < bm.BitInfoHead.biHeight; j += 1)
	{
		for (int i = 0; i < bm.BitInfoHead.biWidth; i += 1)
		{
			RGBQUAd rgbb = GetPix(&bm, i, j);
			if (sqrt(pow((double)(m - i), 2) + pow((double)(n - j), 2)) <50)
			{
				if (i % 2 == 0 && j % 2 == 1)
					putpixel(i + m1 - m, j + n1 - n, RGB(rgbb.rgbRed, rgbb.rgbGreen, rgbb.rgbBlue));
			}
		}
	}

	RGBQUAd rgbp;

	CloseBM_pz(bm);	//关闭位图
	getchar();
	getchar();
	closegraph();
	printf("\n");
}

